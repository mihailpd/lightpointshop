﻿using LightpointShop.DAL.Infrastracture.Models;
using LightpointShop.DAL.Infrastracture.Repositories;
using LightpointShop.Domain.Implementation.Services;
using Moq;
using NUnit.Framework;

namespace LightpointShop.Domain.Implementation.Tests
{
    [TestFixture]
    public class ProductTests
    {
        [Test]
        public void AddProduct_CoorectProduct_ProductCreated()
        {
            var productRepository = new Mock<IProductRepository>();
            //productRepository.Setup(t => t.GetId()).Returns(() => i).Callback(() => i++);
            var ps = new ProductService(productRepository.Object);
            ps.AddProduct(new Product
            {
                Name = "name",
                Description = "desc"
            });
            productRepository.Verify(m => m.Create(It.IsNotNull<Product>()));
            productRepository.Verify(m => m.Create(It.IsAny<Product>()));
            productRepository.Verify(m => m.Create(It.Is<Product>(x => x.Name == "name")));
            productRepository.Verify(m => m.Create(It.Is<Product>(x => x.Description == "desc")));
        }
    }
}
