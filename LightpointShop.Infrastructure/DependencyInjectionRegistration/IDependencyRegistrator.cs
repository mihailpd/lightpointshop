﻿using Autofac;

namespace LightpointShop.Infrastructure.DependencyInjectionRegistration
{
    public interface IDependencyRegistrator
    {
        void Register(ContainerBuilder containerBuilder);
    }
}
