﻿using LightpointShop.DAL.Infrastracture.Models;

namespace LightpointShop.DAL.Infrastracture.Repositories
{
    public interface IShopRepository : IBaseRepository<Shop>
    {
         
    }
}