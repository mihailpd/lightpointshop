﻿using System.Collections.Generic;
using LightpointShop.DAL.Infrastracture.Models;

namespace LightpointShop.DAL.Infrastracture.Repositories
{
    public interface IShopProductRepository : IBaseRepository<ShopProduct> { }
}