﻿namespace LightpointShop.DAL.Infrastracture.Models
{
    public abstract class BaseEntity
    {
        public int Id { get; set; }
    }
}