using System.Collections.Generic;

namespace LightpointShop.DAL.Infrastracture.Models
{
    public partial class Product : BaseEntity
    {
        public string Name { get; set; }

        public string Description { get; set; }

        public bool Deleted { get; set; }
        
        public virtual ICollection<ShopProduct> ShopProducts { get; set; }
    }
}
