namespace LightpointShop.DAL.Infrastracture.Models
{
    public partial class ShopProduct : BaseEntity
    {
        public int ShopId { get; set; }

        public int ProductId { get; set; }

        public int Quantity { get; set; }

        public virtual Product Product { get; set; }

        public virtual Shop Shop { get; set; }
    }
}
