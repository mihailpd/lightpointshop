using System;
using System.Collections.Generic;

namespace LightpointShop.DAL.Infrastracture.Models
{
    public partial class Shop : BaseEntity
    {
        public string Name { get; set; }

        public string Location { get; set; }

        public TimeSpan TimeOpen { get; set; }

        public TimeSpan TimeClose { get; set; }

        public bool Deleted { get; set; }

        public virtual ICollection<ShopProduct> ShopProducts { get; set; }
    }
}
