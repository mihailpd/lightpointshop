﻿using Autofac;

namespace LightpointShop.DependencyRegistrator
{
    public static class DependencyRegistrator
    {
        public static void Register(ContainerBuilder containerBuilder)
        {
            new LightpointShop.Domain.Implementation.DependencyRegistrator().Register(containerBuilder);
            new LightpointShop.DAL.MsSql.DependencyRegistrator().Register(containerBuilder);
            //new Mssql.DependencyRegistrator().Register(containerBuilder);
        }
    }
}