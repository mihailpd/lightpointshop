﻿using System;
using System.Collections.Generic;
using LightpointShop.DAL.Infrastracture.Models;

namespace LightpointShop.Domain.Infrastructure.Services
{
    public interface IShopService
    {
        IEnumerable<Shop> GetShops();
        Shop GetShop(int id);
        Shop GetShop(string name);
        IEnumerable<Product> GetProducts(int shopId);
        void AddShop(Shop shop);
        void UpdateShop(Shop shop);
        void DeleteShop(int id);
    }
}