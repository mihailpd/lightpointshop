﻿using System.Collections.Generic;
using LightpointShop.DAL.Infrastracture.Models;

namespace LightpointShop.Domain.Infrastructure.Services
{
    public interface IProductService
    {
        IEnumerable<Product> GetProducts();
        Product GetProduct(int id);
        Product GetProduct(string name);
        void AddProduct(Product product);
        void UpdateProduct(Product product);
        void DeleteProduct(int id);
    }
}