﻿using System.Collections.Generic;
using LightpointShop.DAL.Infrastracture.Models;

namespace LightpointShop.Domain.Infrastructure.Services
{
    public interface IShopProductService
    {
        IEnumerable<ShopProduct> GetShopProducts(int shopId);
        ShopProduct GetShopProduct(int shopId, int productId);
        ShopProduct GetShopProduct(int id);
        void AddShopProduct(ShopProduct shopProduct);
        void UpdateShopProduct(ShopProduct shopProduct);
        void DeleteShopProduct(int id);
    }
}