﻿using System;

namespace LightpointShop.Domain.Infrastructure.Exceptions
{
    public abstract class BaseDomainException : Exception
    {
        protected BaseDomainException(string message) : base(message) { }
    }
}