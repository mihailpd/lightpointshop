﻿namespace LightpointShop.Domain.Infrastructure.Exceptions
{
    public class EntityNotExistExeption : BaseDomainException
    {
        public EntityNotExistExeption(string message) : base(message) { }
    }
}