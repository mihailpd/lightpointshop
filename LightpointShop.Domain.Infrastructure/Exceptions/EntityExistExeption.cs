﻿namespace LightpointShop.Domain.Infrastructure.Exceptions
{
    public class EntityExistExeption : BaseDomainException
    {
        public EntityExistExeption(string message) : base(message) { }
    }
}