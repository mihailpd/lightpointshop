﻿namespace LightpointShop.Domain.Infrastructure.Exceptions
{
    public class SameFieldExistException : BaseDomainException
    {
        public SameFieldExistException(string message) : base(message) { }
    }
}