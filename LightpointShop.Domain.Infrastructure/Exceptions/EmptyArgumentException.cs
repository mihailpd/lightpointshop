﻿namespace LightpointShop.Domain.Infrastructure.Exceptions
{
    public class EmptyArgumentException : BaseDomainException
    {
         public EmptyArgumentException(string message) : base(message) { }
    }
}