﻿using System;
using System.Web.Http;
using LightpointShop.DAL.Infrastracture.Models;
using LightpointShop.Domain.Infrastructure.Exceptions;
using LightpointShop.Domain.Infrastructure.Services;
using LightpointShop.Presentation.Models;

namespace LightpointShop.Presentation.Controllers.Api
{
    public class ShopProductsController : ApiController
    {
        private readonly IShopProductService shopProductService;

        public ShopProductsController(IShopProductService shopProductService)
        {
            this.shopProductService = shopProductService;
        }
        
        [HttpGet]
        public IHttpActionResult Get(int id)
        {
            return this.Ok(this.shopProductService.GetShopProducts(id));
        }
        
        [HttpPost]
        public IHttpActionResult Post([FromBody]ShopProductFormModel value)
        {
            if(value == null)
                return this.BadRequest("Nothing came to us :(");

            try
            {
                var shopProduct = new ShopProduct
                {
                    ShopId = value.ShopId,
                    ProductId = value.ProductId,
                    Quantity = value.Quantity
                };
                this.shopProductService.AddShopProduct(shopProduct);
                return this.Ok(this.shopProductService.GetShopProduct(value.Id));
            }
            catch (ArgumentNullException e)
            {
                return this.BadRequest(e.Message);
            }
            catch (ArgumentException e)
            {
                return this.BadRequest(e.Message);
            }
            catch (EmptyArgumentException e)
            {
                return this.BadRequest(e.Message);
            }
            catch (SameFieldExistException e)
            {
                return this.BadRequest(e.Message);
            }
            catch (EntityExistExeption e)
            {
                return this.BadRequest(e.Message);
            }
            catch (Exception e)
            {
                return this.InternalServerError(e);
            }
        }

        [HttpPatch, HttpPut]
        public IHttpActionResult Update([FromBody]ShopProductFormModel value)
        {
            if(value == null)
                return this.BadRequest("Nothing came to us :(");

            try
            {
                var shopProduct = new ShopProduct
                {
                    Id = value.Id,
                    ShopId = value.ShopId,
                    ProductId = value.ProductId,
                    Quantity = value.Quantity
                };
                this.shopProductService.UpdateShopProduct(shopProduct);
                return this.Ok(this.shopProductService.GetShopProducts(value.Id));
            }
            catch (ArgumentNullException e)
            {
                return this.BadRequest(e.Message);
            }
            catch (ArgumentException e)
            {
                return this.BadRequest(e.Message);
            }
            catch (EmptyArgumentException e)
            {
                return this.BadRequest(e.Message);
            }
            catch (SameFieldExistException e)
            {
                return this.BadRequest(e.Message);
            }
            catch (EntityNotExistExeption e)
            {
                return this.BadRequest(e.Message);
            }
            catch (Exception e)
            {
                return this.InternalServerError(e);
            }
        }
        
        [HttpDelete]
        public IHttpActionResult Delete(int id)
        {
            try
            {
                var target = this.shopProductService.GetShopProduct(id);
                this.shopProductService.DeleteShopProduct(id);
                return this.Ok(target);
            }
            catch (EntityNotExistExeption e)
            {
                return this.BadRequest(e.Message);
            }
            catch (Exception e)
            {
                return this.InternalServerError(e);
            }
        }

    }
}
