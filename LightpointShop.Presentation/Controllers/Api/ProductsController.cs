﻿using System;
using System.Web.Http;
using LightpointShop.DAL.Infrastracture.Models;
using LightpointShop.Domain.Infrastructure.Exceptions;
using LightpointShop.Domain.Infrastructure.Services;
using LightpointShop.Presentation.Models;

namespace LightpointShop.Presentation.Controllers.Api
{
    public class ProductsController : ApiController
    {
        private readonly IProductService productService;

        public ProductsController(IProductService productService)
        {
            this.productService = productService;
        }
        
        [HttpGet]
        public IHttpActionResult Get()
        {
            return this.Ok(this.productService.GetProducts());
        }
        
        [HttpGet]
        public IHttpActionResult Get(int id)
        {
            return this.Ok(this.productService.GetProduct(id));
        }
        
        [HttpPost]
        public IHttpActionResult Post([FromBody]ProductFormModel value)
        {
            if(value == null)
                return this.BadRequest("Nothing came to us :(");

            try
            {
                var product = new Product
                {
                    Name = value.Name,
                    Description = value.Description
                };
                this.productService.AddProduct(product);
                return this.Ok(this.productService.GetProduct(value.Name));
            }
            catch (ArgumentNullException e)
            {
                return this.BadRequest(e.Message);
            }
            catch (EmptyArgumentException e)
            {
                return this.BadRequest(e.Message);
            }
            catch (SameFieldExistException e)
            {
                return this.BadRequest(e.Message);
            }
            catch (Exception e)
            {
                return this.InternalServerError(e);
            }
        }

        [HttpPatch, HttpPut]
        public IHttpActionResult Update([FromBody]ProductFormModel value)
        {
            if(value == null)
                return this.BadRequest("Nothing came to us :(");

            try
            {
                var product = new Product
                {
                    Id = value.Id,
                    Name = value.Name,
                    Description = value.Description
                };
                this.productService.UpdateProduct(product);
                return this.Ok(this.productService.GetProduct(value.Id));
            }
            catch (ArgumentNullException e)
            {
                return this.BadRequest(e.Message);
            }
            catch (EmptyArgumentException e)
            {
                return this.BadRequest(e.Message);
            }
            catch (SameFieldExistException e)
            {
                return this.BadRequest(e.Message);
            }
            catch (EntityNotExistExeption e)
            {
                return this.BadRequest(e.Message);
            }
            catch (Exception e)
            {
                return this.InternalServerError(e);
            }
        }
        
        [HttpDelete]
        public IHttpActionResult Delete(int id)
        {
            try
            {
                var target = this.productService.GetProduct(id);
                this.productService.DeleteProduct(id);
                return this.Ok(target);
            }
            catch (EntityNotExistExeption e)
            {
                return this.BadRequest(e.Message);
            }
            catch (Exception e)
            {
                return this.InternalServerError(e);
            }
        }

    }
}
