﻿using System;
using System.Web.Http;
using LightpointShop.DAL.Infrastracture.Models;
using LightpointShop.Domain.Infrastructure.Exceptions;
using LightpointShop.Domain.Infrastructure.Services;
using LightpointShop.Presentation.Models;

namespace LightpointShop.Presentation.Controllers.Api
{
    public class ShopsController : ApiController
    {
        private readonly IShopService shopService;

        public ShopsController(IShopService shopService)
        {
            this.shopService = shopService;
        }
        
        [HttpGet]
        public IHttpActionResult Get()
        {
            return this.Ok(this.shopService.GetShops());
        }
        
        [HttpGet]
        public IHttpActionResult Get(int id)
        {
            return this.Ok(this.shopService.GetShop(id));
        }
        
        [HttpPost]
        public IHttpActionResult Post([FromBody]ShopFormModel value)
        {
            if(value == null)
                return this.BadRequest("Nothing came to us :(");

            try
            {
                var shop = new Shop
                {
                    Name = value.Name,
                    Location = value.Location,
                    TimeOpen = value.TimeOpen,
                    TimeClose = value.TimeClose
                };
                this.shopService.AddShop(shop);
                return this.Ok(this.shopService.GetShop(value.Name));
            }
            catch (ArgumentNullException e)
            {
                return this.BadRequest(e.Message);
            }
            catch (EmptyArgumentException e)
            {
                return this.BadRequest(e.Message);
            }
            catch (SameFieldExistException e)
            {
                return this.BadRequest(e.Message);
            }
            catch (Exception e)
            {
                return this.InternalServerError(e);
            }
        }
        
        [HttpPatch, HttpPut]
        public IHttpActionResult Update([FromBody]ShopFormModel value)
        {
            if(value == null)
                return this.BadRequest("Nothing came to us :(");

            try
            {
                var shop = new Shop
                {
                    Id = value.Id,
                    Name = value.Name,
                    Location = value.Location,
                    TimeOpen = value.TimeOpen,
                    TimeClose = value.TimeClose
                };
                this.shopService.UpdateShop(shop);
                return this.Ok(this.shopService.GetShop(value.Id));
            }
            catch (ArgumentNullException e)
            {
                return this.BadRequest(e.Message);
            }
            catch (EmptyArgumentException e)
            {
                return this.BadRequest(e.Message);
            }
            catch (SameFieldExistException e)
            {
                return this.BadRequest(e.Message);
            }
            catch (EntityNotExistExeption e)
            {
                return this.BadRequest(e.Message);
            }
            catch (Exception e)
            {
                return this.InternalServerError(e);
            }
        }
        
        [HttpDelete]
        public IHttpActionResult Delete(int id)
        {
            try
            {
                var target = this.shopService.GetShop(id);
                this.shopService.DeleteShop(id);
                return this.Ok(target);
            }
            catch (EntityNotExistExeption e)
            {
                return this.BadRequest(e.Message);
            }
            catch (Exception e)
            {
                return this.InternalServerError(e);
            }
        }

    }
}
