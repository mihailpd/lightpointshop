﻿using System.Web.Mvc;
using LightpointShop.Domain.Infrastructure.Services;

namespace LightpointShop.Presentation.Controllers
{
    public class ShopsController : Controller
    {
        private readonly IShopService shopService;

        public ShopsController(IShopService shopService)
        {
            this.shopService = shopService;
        }

        public ActionResult Index()
        {
            this.ViewBag.Title = "Shops";
            this.ViewBag.PageTitle = "Shops List";
            
            return View(this.shopService.GetShops());
        }
    }
}
