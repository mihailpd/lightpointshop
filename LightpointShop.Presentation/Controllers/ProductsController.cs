﻿using System.Web.Mvc;
using LightpointShop.Domain.Infrastructure.Services;

namespace LightpointShop.Presentation.Controllers
{
    public class ProductsController : Controller
    {
        private readonly IProductService productService;

        public ProductsController(IProductService productService)
        {
            this.productService = productService;
        }

        public ActionResult Index()
        {
            this.ViewBag.Title = "Products";
            this.ViewBag.PageTitle = "Products List";

            return View(this.productService.GetProducts());
        }
    }
}
