﻿using System;
using System.ComponentModel;

namespace LightpointShop.Presentation.Models
{
    public class ShopFormModel
    {
        public int Id { get; set; }

        [DisplayName("Shop name")]
        public string Name { get; set; }

        [DisplayName("Shop address")]
        public string Location { get; set; }

        [DisplayName("Opening time")]
        public TimeSpan TimeOpen { get; set; }

        [DisplayName("Closing time")]
        public TimeSpan TimeClose { get; set; }
    }
}