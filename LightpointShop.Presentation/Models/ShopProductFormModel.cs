﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web.Mvc;
using LightpointShop.Domain.Infrastructure.Services;
using LightpointShop.Presentation.Controllers.Api;

namespace LightpointShop.Presentation.Models
{
    public class ShopProductFormModel
    {
        public int Id { get; set; }
        [DisplayName("Shop")]
        public int ShopId { get; set; }
        [DisplayName("Product")]
        public int ProductId { get; set; }
        public int Quantity { get; set; }
       
    }
}