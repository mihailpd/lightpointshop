﻿namespace LightpointShop.Presentation.Models
{
    public class ProductFormModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
    }
}