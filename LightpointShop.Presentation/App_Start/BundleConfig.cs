﻿using System.Web.Optimization;

namespace LightpointShop.Presentation
{
    public class BundleConfig
    {
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-1.10.2.min.js",
                        "~/Scripts/jquery.unobtrusive-ajax.js"));

            bundles.Add(new ScriptBundle("~/bundles/material").Include(
                        "~/Scripts/material.min.js",
                        "~/Scripts/material-selectlist.js"));

            bundles.Add(new ScriptBundle("~/bundles/site").Include(
                        "~/Scripts/site-scripts.js"));

            //bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
            //            "~/Scripts/jquery.validate-vsdoc.js",
            //            "~/Scripts/jquery.validate.js",
            //            "~/Scripts/jquery.validate.unobtrusive.js"));



            bundles.Add(new StyleBundle("~/Content/fonts").Include(
                      "~/Content/fonts.css"));
            
            bundles.Add(new StyleBundle("~/Content/material").Include(
                      "~/Content/material-icons.css",
                      "~/Content/material.css",
                      "~/Content/material-selectlist.css"));
            
            bundles.Add(new StyleBundle("~/Content/site").Include(
                      "~/Content/styles.css"));
        }
    }
}
