﻿using System.Reflection;
using System.Web.Http;
using System.Web.Mvc;
using Autofac;
using Autofac.Integration.Mvc;
using Autofac.Integration.WebApi;

namespace LightpointShop.Presentation
{
    public static class DependencyInjectionConfig
    {
        public static void RegisterDependencyInjection(HttpConfiguration configuration)
        {
            RegisterMvc(configuration); 
            RegisterWebApi(configuration); 
        }

        private static void RegisterWebApi(HttpConfiguration configuration)
        {
            var builder = new ContainerBuilder();
            DependencyRegistrator.DependencyRegistrator.Register(builder);
            builder.RegisterApiControllers(Assembly.GetExecutingAssembly());
            builder.RegisterWebApiFilterProvider(configuration);
            IContainer container = builder.Build();
            configuration.DependencyResolver = new AutofacWebApiDependencyResolver(container);

        }

        private static void RegisterMvc(HttpConfiguration configuration)
        {
            var builder = new ContainerBuilder();
            builder.RegisterControllers(Assembly.GetCallingAssembly());
            builder.RegisterFilterProvider();
            builder.RegisterSource(new ViewRegistrationSource());
            DependencyRegistrator.DependencyRegistrator.Register(builder);
            var container = builder.Build();
            DependencyResolver.SetResolver(new AutofacDependencyResolver(container));

        }
    }
}