﻿
$(document).on("click", ".dialog-button", (e) => {
    var openId = $(e.currentTarget).attr("for");
    var dialog = document.querySelector("#" + openId);
    dialog.showModal();
    dialog.querySelector("button:not([disabled])").addEventListener("click", () => {
        dialog.close();
    });
});
$(document).on("click", "button[for=add-shop-product-dialog]", (e) => {
    var shopId = this.getShopId(e.currentTarget);
    var productId = this.getProductId(e.currentTarget);
    var shopsList = $("#ShopId");
    var productsList = $("#ProductId");
    
    if (shopId == undefined) {
        $.ajax({
            url: "/api/shops/",
            type: "GET",
            error: (result) => {
                ajaxFailed(result);
            },
            success: (result) => {
                if (result.length < 1) {
                    document.querySelector("#add-shop-product-dialog").close();
                    this.showMessage("No shops added");
                    return;
                }
                shopsList.html(this.getSelectListHtml(result));
                shopsList.parent().addClass("is-dirty");
                shopsList.parent().css("display", "block");
            }
        });
    } else {
        shopsList.html('<option value="' + shopId + '" />');
        shopsList.parent().css("display", "none");
    }
    if (productId == undefined) {
        $.ajax({
            url: "/api/products/",
            type: "GET",
            error: (result) => {
                ajaxFailed(result);
            },
            success: (result) => {
                if (result.length < 1) {
                    document.querySelector("#add-shop-product-dialog").close();
                    this.showMessage("No products added");
                    return;
                }
                productsList.html(this.getSelectListHtml(result));
                productsList.parent().addClass("is-dirty");
                productsList.parent().css("display", "block");
            }
        });
    } else {
        productsList.html('<option value="' + productId + '" />');
        productsList.parent().css("display", "none");
    }
});

$(document).on("click", ".show-products-list", (e) => {
    var shopId = this.getShopId(e.currentTarget);

    $("#products-preloader").css("display", "block");
    $("#show-products-data").html("");
    $.ajax({
        url: "/api/shops/" + shopId,
        type: "GET",
        error: (result) => {
            ajaxFailed(result);
        },
        success: (result) => {
            $("#products-preloader").css("display", "none");
            $("#show-products-data").html(this.getShopProductsHtml(result.ShopProducts));
        }
    });
});

$(document).on("click", ".remove-shop", (e) => {
    var shopId = this.getShopId(e.currentTarget);
    var dialog = document.querySelector("#delete-dialog");
    $(dialog).attr("data-shop-id", shopId);
});
$(document).on("click", "#remove-shop-confirmed", (e) => {
    var dialog = document.querySelector("#delete-dialog");
    var shopId = $(dialog).attr("data-shop-id");
    $.ajax({
        url: "/api/shops/" + shopId,
        type: "DELETE",
        error: (result) => {
            ajaxFailed(result);
        },
        success: (result) => {
            dialog.close();
            $("#shop-" + shopId).remove();
        }
    });
});

$(document).on("click", ".remove-product", (e) => {
    var productId = this.getProductId(e.currentTarget);
    var dialog = document.querySelector("#delete-dialog");
    $(dialog).attr("data-product-id", productId);
});
$(document).on("click", "#remove-product-confirmed", (e) => {
    var dialog = document.querySelector("#delete-dialog");
    var productId = $(dialog).attr("data-product-id");
    $.ajax({
        url: "/api/products/" + productId,
        type: "DELETE",
        error: (result) => {
            ajaxFailed(result);
        },
        success: (result) => {
            dialog.close();
            $("#product-" + productId).remove();
        }
    });
});

$(document).on("click", ".remove-shop-product", (e) => {
    var shopProductId = this.getShopProductId(e.currentTarget);
    $.ajax({
        url: "/api/shopproducts/" + shopProductId,
        type: "DELETE",
        error: (result) => {
            ajaxFailed(result);
        },
        success: (result) => {
            $("#shop-product-" + shopProductId).remove();
        }
    });
});

var getShopId = (element) => {
    return $(element).attr("data-shop-id");
};
var getShopProductId = (element) => {
    return $(element).attr("data-shop-product-id");
};
var getProductId = (element) => {
    return $(element).attr("data-product-id");
};

var ajaxFailed = (e) => {
    var object = JSON.parse(e.responseText);
    var message;
    if (object.Message) {
        message = object.Message;
    } else if (e.statusText != null) {
        message = "Error " + e.status + ": " + e.statusText;
    } else {
        message = "Please check your internet connection!";
    }
    this.showMessage(message);
};
var showMessage = (message) => {
    var notification = document.querySelector(".mdl-js-snackbar");
    notification.MaterialSnackbar.showSnackbar({ message: message });
};

var getShopProductsHtml = (products) => {
    if (products.length <= 0) {
        return '<div class="center">Empty products list</div>';
    } else {
        var html = '<ul class="mdl-list">';
        $.each(products, (i, el) => {
            html += this.getShopProductHtml(el.Id, el.Product.Name, el.Quantity);
        });
        html += '</ul>';
        return html;
    }
};
var getShopProductHtml = (id, name, quantity) => {
    return '    <li id="shop-product-'+ id + '" class="mdl-list__item"> \
                    <span class="mdl-list__item-primary-content">' + name + ' (' + quantity + ')</span> \
                    <span class="mdl-list__item-secondary-content"> \
                        <button class="mdl-button mdl-js-button mdl-js-ripple-effect mdl-button--icon remove-shop-product" for="delete-dialog" data-shop-product-id="' + id + '"> \
                            <i class="material-icons">delete</i> \
                        </button> \
                    </span> \
                </li>';
};
var getShopHtml = (id, name, location, timeFrom, timeTo) => {
    return '<li id="shop-' + id + '" class="mdl-list__item mdl-list__item--three-line mdl-card-custom mdl-shadow--2dp">'
                + getShopInnerHtml(id, name, location, timeFrom, timeTo) +
            '</li>';
};
var getShopInnerHtml = (id, name, location, timeFrom, timeTo) => {
    if (location == null) { location = ""; }
    return '<span class="mdl-list__item-primary-content"> \
                    <i class="material-icons  mdl-list__item-avatar">store</i> \
                    <span>' + name + '</span> \
                    <span class="mdl-list__item-text-body"> \
                        ' + location + ' \
                        <p>open from ' + timeFrom + ' to ' + timeTo + '</p> \
                    </span> \
                </span> \
                <div class="mdl-layout-spacer"></div> \
                <button class="mdl-button mdl-js-button mdl-js-ripple-effect mdl-button--icon show-products-list dialog-button" for="show-products-dialog" data-shop-id="' + id + '"> \
                    <i class="material-icons">remove_red_eye</i> \
                </button> \
                <button class="mdl-button mdl-js-button mdl-js-ripple-effect mdl-button--icon remove-shop dialog-button" for="delete-dialog" data-shop-id="' + id + '"> \
                    <i class="material-icons">delete</i> \
                </button>';
};
var newShopSuccess = (e) => {
    if ($(".shops-list > li").length < 1) {
        $(".shops-list").html("");
    }
    $(".shops-list").append(getShopHtml(e.Id, e.Name, e.Location, e.TimeOpen, e.TimeClose));
    document.querySelector("#create-shop-dialog").close();
    this.showMessage("Shop added.");
};

var getProductHtml = (id, name, description) => {
    if (description == null) {
        description = "";
    }
    return '<li id="product-' + id + '" class="mdl-list__item mdl-list__item--two-line mdl-card-custom mdl-shadow--2dp"> \
                <span class="mdl-list__item-primary-content"> \
                    <i class="material-icons mdl-list__item-avatar">local_offer</i> \
                    <span>' + name + '</span> \
                    <span class="mdl-list__item-sub-title">' + description + '</span> \
                </span> \
                <div class="mdl-layout-spacer"></div> \
                <button class="mdl-button mdl-js-button mdl-js-ripple-effect mdl-button--icon remove-product dialog-button" for="add-shop-product-dialog" data-product-id="' + id + '"> \
                    <i class="material-icons">add_circle</i> \
                </button> \
                <button class="mdl-button mdl-js-button mdl-js-ripple-effect mdl-button--icon remove-product dialog-button" for="delete-dialog" data-product-id="' + id + '"> \
                    <i class="material-icons">delete</i> \
                </button> \
            </li>';
};
var newProductSuccess = (e) => {
    if ($(".products-list > li").length < 1) {
        $(".products-list").html("");
    }
    $(".products-list").append(getProductHtml(e.Id, e.Name, e.Description));
    document.querySelector("#create-product-dialog").close();
    this.showMessage("Product added.");
};

var getSelectListHtml = (list) => {
    var html = "";

    $.each(list, (i, el) => {
        html += '<option value="' + el.Id + '">' + el.Name + '</option>';
    });

    return html;
};
var newShopProductSuccess = (e) => {
    document.querySelector("#add-shop-product-dialog").close();
    this.showMessage("Product added to the shop.");
};