﻿using Autofac;
using LightpointShop.DAL.Infrastracture.Repositories;
using LightpointShop.DAL.MsSql.Context;
using LightpointShop.DAL.MsSql.Repositories;
using LightpointShop.Infrastructure.DependencyInjectionRegistration;

namespace LightpointShop.DAL.MsSql
{
    public class DependencyRegistrator : IDependencyRegistrator
    {
        public void Register(ContainerBuilder containerBuilder)
        {
            containerBuilder.RegisterType<DatabaseContext>().AsSelf().InstancePerLifetimeScope();

            containerBuilder.RegisterType<ProductRepository>().As<IProductRepository>();
            containerBuilder.RegisterType<ShopRepository>().As<IShopRepository>();
            containerBuilder.RegisterType<ShopProductRepository>().As<IShopProductRepository>();
        }
    }
}