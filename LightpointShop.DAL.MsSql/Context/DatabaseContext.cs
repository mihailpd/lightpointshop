using System.Data.Entity;
using LightpointShop.DAL.Infrastracture.Models;

namespace LightpointShop.DAL.MsSql.Context
{
    public partial class DatabaseContext : DbContext
    {
        public DatabaseContext() : base("name=DbConnectionString")
        {
            var ensureDllIsCopied = System.Data.Entity.SqlServer.SqlProviderServices.Instance;
        }

        public virtual DbSet<Product> Products { get; set; }
        public virtual DbSet<ShopProduct> ShopProducts { get; set; }
        public virtual DbSet<Shop> Shops { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            //modelBuilder.Entity<ShopProduct>().HasKey(e => new { e.ShopId, e.ProductId });

            modelBuilder.Entity<Product>()
                .HasMany(e => e.ShopProducts)
                .WithRequired(e => e.Product)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Shop>()
                .HasMany(e => e.ShopProducts)
                .WithRequired(e => e.Shop)
                .WillCascadeOnDelete(false);
        }
    }
}
