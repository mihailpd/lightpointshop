﻿using System.Data.Entity;
using System.Linq;
using LightpointShop.DAL.Infrastracture.Repositories;
using LightpointShop.DAL.MsSql.Context;
using Product = LightpointShop.DAL.Infrastracture.Models.Product;

namespace LightpointShop.DAL.MsSql.Repositories
{
    public class ProductRepository : BaseRepository<Product>, IProductRepository
    {
        public ProductRepository(DatabaseContext databaseContext) : base(databaseContext) { }

        protected override DbSet<Product> GetEntities() => this.databaseContext.Products;

        public override void Update(Product item)
        {
            var target = this.databaseContext.Products.FirstOrDefault(x => x.Id == item.Id);
            if (target == null)
                return;

            target.Description = item.Description;
            target.Name = item.Name;

            this.databaseContext.SaveChanges();
        }

        public override void Delete(int id)
        {
            var target = this.GetEntities().FirstOrDefault(x => x.Id == id);
            if (target == null)
                return;

            target.Deleted = true;
            this.databaseContext.SaveChanges();
        }
    }
}