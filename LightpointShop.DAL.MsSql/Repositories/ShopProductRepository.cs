﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using LightpointShop.DAL.Infrastracture.Models;
using LightpointShop.DAL.Infrastracture.Repositories;
using LightpointShop.DAL.MsSql.Context;
using ShopProduct = LightpointShop.DAL.Infrastracture.Models.ShopProduct;

namespace LightpointShop.DAL.MsSql.Repositories
{
    public class ShopProductRepository : BaseRepository<ShopProduct>, IShopProductRepository
    {
        public ShopProductRepository(DatabaseContext databaseContext) : base(databaseContext) { }

        protected override DbSet<ShopProduct> GetEntities() => this.databaseContext.ShopProducts;

        public override void Update(ShopProduct item)
        {
            var target = this.databaseContext.ShopProducts.FirstOrDefault(x => x.ShopId == item.ShopId && x.ProductId == item.ProductId);
            if (target == null)
                return;

            target.Quantity = item.Quantity;

            this.databaseContext.SaveChanges();
        }

        public override void Delete(int id)
        {
            var target = this.GetEntities().FirstOrDefault(x => x.Id == id);
            if (target == null)
                return;

            this.databaseContext.ShopProducts.Remove(target);
            this.databaseContext.SaveChanges();
        }

    }
}