﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using LightpointShop.DAL.Infrastracture.Models;
using LightpointShop.DAL.Infrastracture.Repositories;
using LightpointShop.DAL.MsSql.Context;
using Shop = LightpointShop.DAL.Infrastracture.Models.Shop;

namespace LightpointShop.DAL.MsSql.Repositories
{
    public class ShopRepository : BaseRepository<Shop>, IShopRepository
    {
        public ShopRepository(DatabaseContext databaseContext) : base(databaseContext) { }

        protected override DbSet<Shop> GetEntities() => this.databaseContext.Shops;

        public override void Update(Shop item)
        {
            var target = this.databaseContext.Shops.FirstOrDefault(x => x.Id == item.Id);
            if (target == null)
                return;

            target.Location = item.Location;
            target.Name = item.Name;
            target.TimeOpen = item.TimeOpen;
            target.TimeClose = item.TimeClose;

            this.databaseContext.SaveChanges();
        }

        public override void Delete(int id)
        {
            var target = this.GetEntities().FirstOrDefault(x => x.Id == id);
            if (target == null)
                return;

            target.Deleted = true;
            this.databaseContext.SaveChanges();
        }
    }
}