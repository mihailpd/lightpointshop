﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using LightpointShop.DAL.Infrastracture.Models;
using LightpointShop.DAL.Infrastracture.Repositories;
using LightpointShop.DAL.MsSql.Context;

namespace LightpointShop.DAL.MsSql.Repositories
{
    public abstract class BaseRepository<T> : IBaseRepository<T> where T : BaseEntity
    {
        protected readonly DatabaseContext databaseContext;
         
        protected BaseRepository(DatabaseContext databaseContext)
        {
            this.databaseContext = databaseContext;
        }

        protected abstract DbSet<T> GetEntities(); 

        public IEnumerable<T> GetAll()
        {
            return this.GetEntities();
        }

        public T Get(int id)
        {
            return this.GetEntities().FirstOrDefault(x => x.Id == id);
        }

        public void Create(T item)
        {
            this.GetEntities().Add(item);
            this.databaseContext.SaveChanges();
        }

        public abstract void Update(T item);

        public abstract void Delete(int id);
    }
}