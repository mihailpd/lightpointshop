﻿using System;
using System.Collections.Generic;
using System.Linq;
using LightpointShop.DAL.Infrastracture.Models;
using LightpointShop.DAL.Infrastracture.Repositories;
using LightpointShop.Domain.Infrastructure.Exceptions;
using LightpointShop.Domain.Infrastructure.Services;

namespace LightpointShop.Domain.Implementation.Services
{
    public class ShopProductService : IShopProductService
    {
        private readonly IShopProductRepository shopProductRepository;

        public ShopProductService(IShopProductRepository shopProductRepository)
        {
            this.shopProductRepository = shopProductRepository;
        }

        public IEnumerable<ShopProduct> GetShopProducts(int shopId)
        {
            return this.shopProductRepository.GetAll().Where(x => x.ShopId == shopId);
        }

        public ShopProduct GetShopProduct(int shopId, int productId)
        {
            return this.shopProductRepository.GetAll().FirstOrDefault(x => x.ShopId == shopId && x.ProductId == productId);
        }

        public ShopProduct GetShopProduct(int id)
        {
            return this.shopProductRepository.Get(id);
        }

        public void AddShopProduct(ShopProduct shopProduct)
        {
            if (shopProduct == null)
                throw new ArgumentNullException(nameof(shopProduct));
            if (shopProduct.ShopId <= 0)
                throw new ArgumentException(nameof(shopProduct.ShopId));
            if (shopProduct.ProductId <= 0)
                throw new ArgumentException(nameof(shopProduct.ProductId));
            if (this.GetShopProduct(shopProduct.ShopId, shopProduct.ProductId) != null)
                throw new EntityExistExeption("This relation is exist.");
            if (shopProduct.Quantity < 0)
                throw new ArgumentException(nameof(shopProduct.Quantity));

            this.shopProductRepository.Create(shopProduct);
        }

        public void UpdateShopProduct(ShopProduct shopProduct)
        {
            if (shopProduct == null)
                throw new ArgumentNullException(nameof(shopProduct));
            if (this.shopProductRepository.Get(shopProduct.Id) == null)
                throw new EntityNotExistExeption("Relation is not exist.");
            if (shopProduct.Quantity < 0)
                throw new ArgumentException(nameof(shopProduct.Quantity));

            this.shopProductRepository.Update(shopProduct);
        }

        public void DeleteShopProduct(int id)
        {
            if (this.shopProductRepository.Get(id) == null)
                throw new EntityNotExistExeption("This relation is not exist.");

            this.shopProductRepository.Delete(id);
        }

    }
}