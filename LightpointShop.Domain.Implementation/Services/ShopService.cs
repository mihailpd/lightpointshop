﻿using System;
using System.Collections.Generic;
using System.Linq;
using LightpointShop.DAL.Infrastracture.Models;
using LightpointShop.DAL.Infrastracture.Repositories;
using LightpointShop.Domain.Infrastructure.Exceptions;
using LightpointShop.Domain.Infrastructure.Services;

namespace LightpointShop.Domain.Implementation.Services
{
    public class ShopService : IShopService
    {
        private readonly IShopRepository shopRepository;

        public ShopService(IShopRepository shopRepository)
        {
            this.shopRepository = shopRepository;
        }
        
        public IEnumerable<Shop> GetShops()
        {
            return this.shopRepository.GetAll().Where(x => !x.Deleted);
        }

        public IEnumerable<Shop> GetShopsWithDeleted()
        {
            return this.shopRepository.GetAll();
        }

        public Shop GetShop(int id)
        {
            var result = this.shopRepository.Get(id);
            if (result.Deleted)
                return null;

            return result;
        }

        public Shop GetShop(string name)
        {
            return this.shopRepository.GetAll().FirstOrDefault(x => x.Name == name);
        }

        public IEnumerable<Product> GetProducts(int shopId)
        {
            var shop = this.shopRepository.Get(shopId);
            if (shop.Deleted)
                return null;

            return shop.ShopProducts.Select(x => x.Product).Where(x => !x.Deleted);
        }

        public void AddShop(Shop shop)
        {
            if (shop == null)
                throw new ArgumentNullException(nameof(shop));
            if (string.IsNullOrWhiteSpace(shop.Name))
                throw new EmptyArgumentException("Shop name can not be empty.");
            if (this.shopRepository.GetAll().Any(x => x.Id != shop.Id && x.Name == shop.Name))
                throw new SameFieldExistException("Shop names can not have same name.");

            this.shopRepository.Create(shop);
        }

        public void UpdateShop(Shop shop)
        {
            if (shop == null)
                throw new ArgumentNullException(nameof(shop));
            if (this.shopRepository.Get(shop.Id) == null)
                throw new EntityNotExistExeption("Shop for updating is not exist.");
            if (string.IsNullOrWhiteSpace(shop.Name))
                throw new EmptyArgumentException("Shop name can not be empty.");
            if (this.shopRepository.GetAll().Any(x => x.Id != shop.Id && x.Name == shop.Name))
                throw new SameFieldExistException("Shop names can not have same name.");

            this.shopRepository.Update(shop);
        }

        public void DeleteShop(int id)
        {
            if (this.shopRepository.Get(id) == null)
                throw new EntityNotExistExeption("This shop is not exist.");

            this.shopRepository.Delete(id);
        }
        
    }
}