﻿using System;
using System.Collections.Generic;
using System.Linq;
using LightpointShop.DAL.Infrastracture.Models;
using LightpointShop.DAL.Infrastracture.Repositories;
using LightpointShop.Domain.Infrastructure.Exceptions;
using LightpointShop.Domain.Infrastructure.Services;

namespace LightpointShop.Domain.Implementation.Services
{
    public class ProductService : IProductService
    {
        private readonly IProductRepository productRepository;

        public ProductService(IProductRepository productRepository)
        {
            if (productRepository == null)
                throw new ArgumentNullException(nameof(productRepository));

            this.productRepository = productRepository;
        }

        public IEnumerable<Product> GetProducts()
        {
            return this.productRepository.GetAll().Where(x => !x.Deleted);
        }

        public IEnumerable<Product> GetProductsWithDeleted()
        {
            return this.productRepository.GetAll();
        }

        public Product GetProduct(int id)
        {
            var result = this.productRepository.Get(id);
            if (result.Deleted)
                return null;

            return result;
        }

        public Product GetProduct(string name)
        {
            return this.productRepository.GetAll().FirstOrDefault(x => x.Name == name && !x.Deleted);
        }

        public void AddProduct(Product product)
        {
            if (product == null)
                throw new ArgumentNullException(nameof(product));
            if (string.IsNullOrWhiteSpace(product.Name))
                throw new EmptyArgumentException("Product name can not be empty.");
            if (this.productRepository.GetAll().Any(x => x.Name == product.Name))
                throw new SameFieldExistException("Products names can not be same name.");

            this.productRepository.Create(product);
        }

        public void UpdateProduct(Product product)
        {
            if (product == null)
                throw new ArgumentNullException(nameof(product));
            if (this.productRepository.Get(product.Id) == null)
                throw new EntityNotExistExeption("Product for updating is not exist.");
            if (string.IsNullOrWhiteSpace(product.Name))
                throw new EmptyArgumentException("Product name can not be empty.");
            if (this.productRepository.GetAll().Any(x => x.Id != product.Id && x.Name == product.Name))
                throw new SameFieldExistException("Products names can not have same name.");

            this.productRepository.Update(product);
        }
        
        public void DeleteProduct(int id)
        {
            if (this.productRepository.Get(id) == null)
                throw new EntityNotExistExeption("Product for deleting is not exist.");

            this.productRepository.Delete(id);
        }
    }
}