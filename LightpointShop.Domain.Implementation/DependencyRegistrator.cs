﻿using Autofac;
using LightpointShop.DAL.Infrastracture.Models;
using LightpointShop.Domain.Implementation.Services;
using LightpointShop.Domain.Infrastructure.Services;
using LightpointShop.Infrastructure.DependencyInjectionRegistration;

namespace LightpointShop.Domain.Implementation
{
    public class DependencyRegistrator : IDependencyRegistrator
    {
        public void Register(ContainerBuilder containerBuilder)
        {
            containerBuilder.RegisterType<ShopService>().As<IShopService>();
            containerBuilder.RegisterType<ProductService>().As<IProductService>();
            containerBuilder.RegisterType<ShopProductService>().As<IShopProductService>();
        }
    }
}
