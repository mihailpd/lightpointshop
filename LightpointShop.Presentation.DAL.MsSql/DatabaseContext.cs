using LightpointShop.DAL.Infrastracture.Models;

namespace LightpointShop.Presentation.DAL.MsSql
{
    using System.Data.Entity;

    public partial class DatabaseContext : DbContext
    {
        public DatabaseContext() : base(DatabaseConnection.ConnectionString) { }

        public virtual DbSet<Product> Products { get; set; }
        public virtual DbSet<Shop> Shops { get; set; }
        public virtual DbSet<ShopProduct> ShopProducts { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Product>()
                .HasMany(e => e.ShopProducts)
                .WithRequired(e => e.Product)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Shop>()
                .HasMany(e => e.ShopProducts)
                .WithRequired(e => e.Shop)
                .WillCascadeOnDelete(false);
        }
    }
}
